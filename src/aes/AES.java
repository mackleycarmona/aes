package aes;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

/**
 *
 * @author Mackley.Carmona
 * @Date 30/07/2020
 */
public class AES {

    //Metodo de entrada Main
    public static void main(String[] args) {

        //Instancia d ela clase para leer consola
        Scanner sc = new Scanner(System.in);

        //Declara variables de entrada
        String textToUse;

        //Ingresa texto a cifrar
        System.out.println("Ingrese texto a modificar: ");
        textToUse = sc.next();

        //Valores dinamicos para encriptacion
        byte[] key_Array = "".getBytes();
        byte[] iv = "".getBytes();

        try {
            Cipher _Cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec ivSpec = new IvParameterSpec(iv);

            Key SecretKey = new SecretKeySpec(key_Array, "AES");
           
                String encrypted = encrypt(textToUse, _Cipher, SecretKey, ivSpec);
                System.out.println(encrypted);
                return;
            
        } catch (Exception e) {
            System.err.println("Error al iniciar el proceso");
        }

    }

    public static String encrypt(String textToEncrypt, Cipher _Cipher, Key secretKey, IvParameterSpec ivSpec) {
        try {
        	//Inicializa la clase _Cipher
            _Cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec);
            //Efectua la encripcion AES
            byte[] bytesEncrypted = _Cipher.doFinal(textToEncrypt.getBytes());
            //Codifica los bytes en base64
            String encrypted = Base64.encode(bytesEncrypted);
            //retirna el texto encriptado
            return encrypted;
        } catch (Exception ex) {
            System.err.println("Error al encriptar: " + ex);
        }
        return null;
    }

}
